﻿Tasks:
<ol> 
<li>
Correct all of the deficiencies in index.html
</li>
<li>
"Sectors" selectbox:
</li>
<ul>
<li>
Add all the entries from the "Sectors" selectbox to database
</li>
<li>
Compose the "Sectors" selectbox using data from database
</li>
</ul>


<li>
Perform the following activities after the "Save" button has been pressed:
</li>
<ul>
<li>
Validate all input data (all fields are mandatory)
</li>
<li>
Store all input data to database (Name, Sectors, Agree to terms)
</li>
<li>
Refill the form using stored data 
</li>
<li>
Allow the user to edit his/her own data during the session
</li> 
</ul>
</ol> 