﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using SelectorReact.Model;
using SelectorReact.Services.Interfaces;

namespace SelectorReact.Controllers.Api
{
    [Route("api/ApiForm")]
    [ApiController]
    public class ApiFormController : ControllerBase
    {
        private readonly IUserService _userService;

        public ApiFormController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpPost]
        public async Task<JsonResult> Index([FromBody] SaveRequestModel saveRequestModel)
        {
            if (!ModelState.IsValid)
            {
                return new JsonResult(JsonConvert.SerializeObject(new ApiResponse
                {
                    HasError = true,
                    Content = "Not Valid Data"
                },
                Formatting.None,
                new JsonSerializerSettings()
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                }));
            }

            var userId = Convert.ToInt32(HttpContext.Session.GetString(Constants.UserId));
            var user = await _userService.AddOrUpdateUserData(saveRequestModel, userId);

            if (user != null && userId < 1)
            {
                HttpContext.Session.SetString(Constants.UserId, Convert.ToString(user.Id));
            }

            return new JsonResult(JsonConvert.SerializeObject(new ApiResponse
            {
                HasError = false,
                Content = string.Empty
            },
            Formatting.None,
            new JsonSerializerSettings()
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            }));
        }
    }
}