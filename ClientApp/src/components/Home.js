import Constants from "../Constants";
import React, { Component } from 'react';
import axios from 'axios';
import { SectorsSelectLists } from './Sector/SectorsSelectLists';
import UserAddField from './UserAddField';

export class Home extends Component {
	static displayName = Home.name;

	constructor() {
		super();
        this.state = {
            user: {},
			sectors: [],
            selectedSectors: null,
            loaded: false,
            warning: false,
            success: false,
            submitted: false,
            has_error: false,
            content: ""
		}
        this.addNewUser = this.addNewUser.bind(this);
        this.selectedSectors = this.selectedSectors.bind(this);
	}

    addNewUser = user => {
        let usr = this.state.user;
        usr.name = user.name;
        this.setState({ user: usr });
    }

    selectedSectors = sector => {
        console.log("selected Sector -> ", sector);
        this.setState({ selectedSectors: sector });
    }

    componentDidMount() {
		axios.get(Constants.API_ENDPOINT_URL + "api/categories")
            .then(res => {
                let responseDataParsed = JSON.parse(res.data);
				this.setState({
                    sectors: responseDataParsed.categories,
                    user: responseDataParsed.user,
                    selectedSectors: responseDataParsed.sectors.map(item => item.SectorId),
                    loaded: true
                });
            })
            .catch(err => {
                console.log("Error: " + err);
			})
	}

    validateSubmittedData = () => {
        if (!this.state.user.agreementToTerms) {
            this.setState({
                has_error: true,
                content: "Agree to terms are required!"
            });
        }
        else if (this.state.selectedSectors.length < 1) {
            this.setState({
                has_error: true,
                content: "Sector minimum required item is 1"
            });
        }
        else if (this.state.user.name != null && this.state.user.name.length < 4) {
            this.setState({
                has_error: true,
                content: "Name minlength is 4 characters"
            });
        }
        else if (!this.checkIsValidData()) {
            this.setState({
                has_error: true,
                content: "Name, Sector and Agree to terms are required!"
            });
        }
        else {
            this.setState({
                has_error: false,
                content: ""
            });
        }
    }

    submitForm = event => {
        this.validateSubmittedData();
        if (this.state.has_error) return;

        this.setState({ submitted: true });
        axios.post(Constants.API_ENDPOINT_URL + "api/ApiForm", {
            name: this.state.user.name,
            sectors: this.state.selectedSectors,
            agreement: this.state.user.agreementToTerms,
        })
        .then(res => {
            let responseDataParsed = JSON.parse(res.data);
            if (responseDataParsed.has_error) {
                this.setState({
                    loaded: true,
                    has_error: responseDataParsed.has_error,
                    content: responseDataParsed.content
                });
            }
            setTimeout(() => this.setState({ submitted: false }), 1000);
		})
        .catch(err => {
            setTimeout(() => this.setState({ submitted: false }), 1000);
		})
    }

    changeAgreementToTerms = event => {
        let user = this.state.user;
        user.agreementToTerms = event.currentTarget.checked;
        this.setState({ user: user });
    }

    checkIsValidData = () => this.state.user.name != null && this.state.selectedSectors.length > 0;

    render() {
        if (!this.state.loaded) return <div>Loading...</div>;
        return (
            <div>
                <div className="alert alert-info" style={{ display: this.state.has_error ? 'block' : 'none' }}>
                    { this.state.content }
                </div>
                <div id="selections-group" className="form-horizontal">
                    <div className="col-md-offset-2 col-md-10">
						Please enter your name and pick the Sector you are currently involved in. 
	                </div>
                    <div className="form-group">
                        <label className="control-label col-sm-2" htmlFor="form-name-inp">Name:</label>
                        <UserAddField user={this.state.user} addNewUser={this.addNewUser} />
		            </div>
                    <div className="form-group">
                        <label className="control-label col-sm-2" htmlFor="form-sectors-inp">Sectors:</label>
                        {this.state.sectors.length > 0 ? <SectorsSelectLists onSelectedSectors={this.selectedSectors} userSelectedSector={this.state.selectedSectors} sectors={this.state.sectors} /> : null}
		            </div>
	                <div className="col-md-offset-2 col-md-10">
                        <div className="form-group">
                            <input id="AgreementToTerms" onChange={this.changeAgreementToTerms} htmlFor="AgreementToTerms" defaultChecked={this.state.user.agreementToTerms} type="checkbox" /> Agree to terms
		                </div>
	                </div>
	                <div className="col-md-offset-2 col-md-3">
                        <div className="form-group">
                            <div style={{ display: (!this.state.submitted ? 'block' : 'none') }}>
                                <button onClick={this.submitForm} minLength="1" id="sbm-inp-btn" className="btn btn-success" style={{ fontStyle: "24px" }}>
                                    Save
                                </button>
                            </div>
                            <div className={this.state.submitted ? 'spinner-border' : 'hidden'} role="status"></div>
		                </div>
	                </div>
	            </div>
			</div>
		);
	}
}
