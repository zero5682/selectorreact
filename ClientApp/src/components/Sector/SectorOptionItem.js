﻿import React, { Component } from 'react';

export default class SectorOptionItem extends Component {
    static displayName = SectorOptionItem.name;

	constructor(props) {
        super(props);

        var { sector, indent } = props;

		this.state = {
            sector: sector, 
            indent: indent,
            indentMultiplier: 10
		}
	}

	render() {
        return (
            <option value={this.state.sector.id}
                style={{ textIndent: this.state.indent * this.state.indentMultiplier + "px" }}>
                {this.state.sector.name}</option>
    	);
	}
}
