﻿import React, { Component } from 'react';
import SectorOptionItem from "./SectorOptionItem";

const selectElementStyle = {
	height: "500px",
}

export class SectorsSelectLists extends Component {
    static displayName = SectorsSelectLists.name;

	constructor(props) {
        super(props);
        var { sectors, onSelectedSectors, userSelectedSector } = props;

        this.state = {
            sectors: sectors,
            onSelectedSectors: onSelectedSectors,
            userSelectedSectorIds: userSelectedSector
        }
	}

    buildSelectList() {
	    let array = [];
        this.state.sectors.forEach(sector => {
            array.push(this.buildParentSector(sector));
            if (sector.child_sector.length < 1) return;
            array.push(this.buildChildSector(sector.child_sector, 2));
        });
        return array;
    }

    buildParentSector = (sector) => <SectorOptionItem key={sector.id} sector={sector} />

    buildChildSector(sectors, indent = 1) {
	    let array = [];
        sectors.forEach(sector => {
            array.push(<SectorOptionItem key={sector.id} sector={sector} indent={indent} />);
            if (sector.child_sector.length < 1) return;
            var newIndent = indent + 1;
            array.push(this.buildChildSector(sector.child_sector, newIndent));
		});
        return array;
    }

    onSelectListChange = event => {
        if ((event.target || event.target.options) == null) return;
        let selectedSectorIds = [...event.target.options].filter(({ selected }) => selected).map(({ value }) => value);
        this.setState({ userSelectedSectorIds: selectedSectorIds});
        this.state.onSelectedSectors(selectedSectorIds);
    }

	render() {
        return (
			<div className="col-sm-10">
                <select multiple={true} value={this.state.userSelectedSectorIds} onChange={this.onSelectListChange} className="form-control" size="5" style={selectElementStyle}>
	            {
		            this.state.sectors.length > 0 ? this.buildSelectList() : null
	            }
	            </select>
            </div>
        );
	}
}
