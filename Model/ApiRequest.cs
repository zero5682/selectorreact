﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SelectorReact.Model
{
    public class ApiRequest
    {
        public bool HasErrors { get; set; }
        public string Content { get; set; }
    }
}
