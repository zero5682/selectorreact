﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace SelectorReact.Model
{
    public class UserSector
    {
        [Key, Column(Order = 0)]
        public int Id { get; set; }

        public int SectorId { get; set; }
        public int UserId { get; set; }

        [JsonIgnore]
        public Sector Sector { get; set; }
        [JsonIgnore]
        public User User { get; set; }
    }
}

