﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace SelectorReact.Model
{
    public class Sector
    {
        [Key, Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("parent_id")]
        [ForeignKey("Id"), Column(Order = 1)]
        public int? ParentId { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("parent")]
        public virtual Sector Parent { get; set; }

        [JsonProperty("child_sector")]
        public virtual List<Sector> ChildSectors { get; set; }

        [JsonProperty("user_sector")]
        public virtual ICollection<UserSector> UserSectors { get; set; }
    }
}
