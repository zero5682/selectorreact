﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SelectorReact.Model
{
    public class User
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key, Column(Order = 0)]
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [NotMapped]
        public List<int> Sectors { get; set; }

        [JsonProperty("agreementToTerms")]
        public bool AgreementToTerms { get; set; } = false;

        public virtual ICollection<UserSector> UserSectors { get; set; }
    }
}
