﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SelectorReact.Model
{
    public class SaveRequestModel
    {
        [Required]
        [MinLength(4)]
        [JsonProperty("name")]
        public string Name { get; set; }

        [Required]
        [JsonProperty("sectors")]
        public List<int> Sectors { get; set; }

        [Required]
        [JsonProperty("agreement")]
        public bool AgreementToTerms { get; set; }
    }
}
