﻿using System;
using SelectorReact.Data;
using SelectorReact.Helper;
using SelectorReact.Model;
using SelectorReact.Services.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace SelectorReact.Services
{
    public class SectorService : ISectorService
    {
        private readonly HelmesSelectorContext _helmesSelectorContext;

        public SectorService(HelmesSelectorContext helmesSelectorContext)
        {
            _helmesSelectorContext = helmesSelectorContext;
        }

        public async Task<List<UserSector>> GetAllSectors(int id)
        {
            try
            {
                return await _helmesSelectorContext.UserSector.Where(x => x.UserId == id).ToListAsync();
            }
            catch (Exception e)
            {
                ExceptionHelper.HandleException(e);
                return new List<UserSector>();
            }
        }
    }
}
