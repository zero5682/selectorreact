﻿using System;
using SelectorReact.Data;
using SelectorReact.Helper;
using SelectorReact.Model;
using SelectorReact.Services.Interfaces;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace SelectorReact.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly HelmesSelectorContext _helmesSelectorContext;

        public CategoryService(HelmesSelectorContext helmesSelectorContext)
        {
            _helmesSelectorContext = helmesSelectorContext;
        }

        public List<Sector> GetCategoryModels()
        {
            try
            {
                return _helmesSelectorContext.Sector.Include(x => x.ChildSectors).OrderBy(x => x.Name)
                    .AsEnumerable().Where(x => x.ParentId == null).ToList();
            }
            catch (Exception e)
            {
                ExceptionHelper.HandleException(e);
                return new List<Sector>();
            }
        }
    }
}
