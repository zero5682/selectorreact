﻿using System;
using SelectorReact.Data;
using SelectorReact.Helper;
using SelectorReact.Model;
using SelectorReact.Services.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SelectorReact.Services
{
    public class UserService : IUserService
    {
        private readonly HelmesSelectorContext _helmesSelectorContext;

        public UserService(HelmesSelectorContext helmesSelectorContext)
        {
            _helmesSelectorContext = helmesSelectorContext;
        }

        public User GetUserData(int userId)
        {
            try
            {
                if (userId < 1) return new User();
                var userModel = _helmesSelectorContext.User.Find(userId);
                return userModel ?? new User();
            }
            catch (Exception e)
            {
                ExceptionHelper.HandleException(e);
                return new User();
            }
        }

        public async Task<User> AddOrUpdateUserData(SaveRequestModel saveRequestModel, int userId)
        {
            try
            {
                var user = new User()
                {
                    Name = saveRequestModel.Name,
                    AgreementToTerms = saveRequestModel.AgreementToTerms,
                    UserSectors = new List<UserSector>()
                };

                foreach (var sectorId in saveRequestModel.Sectors)
                {
                    user.UserSectors.Add(new UserSector() { SectorId = sectorId });
                }

                User userData = GetUserData(userId);
                return (userData.Id == 0) ? await AddUserModelAsync(user) : await UpdateUserModelAsync(user, userData);
            }
            catch (Exception e)
            {
                ExceptionHelper.HandleException(e);
                return new User();
            }
        }

        public async Task<User> AddUserModelAsync(User user)
        {
            try
            {
                await _helmesSelectorContext.User.AddAsync(user);
                await _helmesSelectorContext.SaveChangesAsync();
                return user;
            }
            catch (Exception e)
            {
                ExceptionHelper.HandleException(e);
                return new User();
            }
        }

        public async Task<User> UpdateUserModelAsync(User user, User old)
        {
            try
            {
                old.Name = user.Name;
                old.AgreementToTerms = user.AgreementToTerms;
                old.UserSectors = user.UserSectors;

                var userSectors = _helmesSelectorContext.UserSector.Where(x => x.UserId == old.Id).ToList();
                _helmesSelectorContext.UserSector.RemoveRange(userSectors);

                _helmesSelectorContext.User.Update(old);
                await _helmesSelectorContext.SaveChangesAsync();
                return old;
            }
            catch (Exception e)
            {
                ExceptionHelper.HandleException(e);
                return new User();
            }
        }
    }
}
