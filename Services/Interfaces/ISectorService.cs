﻿using SelectorReact.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SelectorReact.Services.Interfaces
{
    public interface ISectorService
    {
        Task<List<UserSector>> GetAllSectors(int id);
    }
}
