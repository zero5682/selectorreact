﻿using System.Threading.Tasks;
using SelectorReact.Model;

namespace SelectorReact.Services.Interfaces
{
    public interface IUserService
    {
        User GetUserData(int userId);
        Task<User> AddOrUpdateUserData(SaveRequestModel saveRequestModel, int userId);
    }
}
